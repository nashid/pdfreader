/* eslint-disable no-unused-vars */
import { splitPDFInPages } from '../utils'

export default function () {
  const cmd = {}
  cmd.command = 'split <pdfFile> <destFolder>'
  cmd.desc = 'divide pdfFile en un archivo por página y los deja en destFolder'
  cmd.example = 'pdfReader scp pdfReader parse /home/jpsala/Documents/recibos/parsed'
  cmd.builder = yargs => {
    yargs.positional('pdfFile', {
      type: 'string',
      desc: 'PDF a dividir en páginas',
      default: undefined
    })
    yargs.positional('destFolder', {
      type: 'string',
      desc: 'Carpeta donde guardar los pdf generados',
      default: undefined
    })
  }
  cmd.handler = async function (argv) {
    const pdfFile = argv.pdfFile
    const destFolder = argv.destFolder

    const pages = await splitPDFInPages(pdfFile, destFolder)
    console.log('pages', pages, pages.length)
  }
  return cmd
}
