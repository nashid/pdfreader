/* eslint-disable no-unused-vars */
const del = require('del')
const parsedFiles = []
export default function (dep) {
  const dirnameExportImg = '/home/jpsala/Documents/recibos'
  const cmd = {}
  cmd.command = 'parse <source_dir> <target_dir> [--borrar_primero]'
  cmd.desc = 'parse a pdf'
  cmd.example = 'pdfReader parse /home/jpsala/Documents/recibos /home/jpsala/Documents/recibos/parsed -b=1'
  cmd.builder = yargs => {
    yargs.option('borrar_primero', {
      alias: 'b',
      default: false
    })
    yargs.positional('source_dir', {
      type: 'string',
      desc: 'Carpeta donde enstán los pdf\'s a procesar',
      default: undefined
    })
    yargs.positional('target_dir', {
      type: 'string',
      desc: 'carpeta destino',
      default: undefined
    })
  }
  const fs = require('fs')
  const renameFiles = (files, targetDir) => {
    files.forEach(f => {
      const targetFile = `${targetDir}/${f.data.legajo}_${f.data.sublegajo}.pdf`
      fs.copyFileSync(`${f.fileName}`, targetFile)
      console.log('Copiado', targetFile)
    })
  }
  const pdf = require('pdf-parse')
  const parsePdf = async (fileName) => {
    const dataBuffer = fs.readFileSync(fileName)
    return pdf(dataBuffer).then(function (data) {
      const text = data.text.split('\n')
      const find = text.find(l => {
        if (l.includes('/')) {
          const parts = l.split('/')
          if (parts.length === 2) {
            const legajo = Number(parts[0].trim())
            const sublegajo = Number(parts[1].trim())
            if (Number.isInteger(legajo) && Number.isInteger(sublegajo)) {
              // console.log('detalle', { fileName, datos: { legajo, sublegajo } })
              parsedFiles.push({ fileName, data: { legajo, sublegajo } })
              return true
            }
          }
        }
      })
      console.log('find', find)
    })
  }
  cmd.handler = async function (argv) {
    const sourceDir = argv.source_dir
    const targetDir = argv.target_dir
    const borrarPrimero = argv.borrar_primero
    const pdfFiles = fs.readdirSync(sourceDir)
    const promises = pdfFiles.map(
      async (file) => {
        if (file.includes('pdf')) {
          console.log('parsing', `${sourceDir}/${file}`)
          try {
            await parsePdf(`${sourceDir}/${file}`)
          } catch (error) {
            console.log('error parsing ', `${sourceDir}/${file}`)
          }
        }
      }
    )
    Promise.all(promises).then(async () => {
      if (borrarPrimero) {
        const borrado = await del(targetDir + '/*.pdf', { force: true })
        console.log('borrados', borrado)
      }
      renameFiles(parsedFiles, targetDir)
    })
  }
  return cmd
}
