import importDir from 'import-dir'

export default function (yargs) {
  const commands = importDir('./commands')
  Object.keys(commands).forEach(cmd => {
    if (typeof (commands[cmd]) === 'function') { yargs.command(commands[cmd]()) }
  })
}

const { splitPDF } = require('pdf-toolz/SplitCombine')
const fs = require('mz/fs')
// const parse = () => {
//   const sourceDir = argv.source_dir
//   const targetDir = argv.target_dir
//   const borrarPrimero = argv.borrar_primero
//   const pdfFiles = fs.readdirSync(sourceDir)
//   const promises = pdfFiles.map(
//     async (file) => {
//       if (file.includes('pdf')) {
//         await parsePdf(`${sourceDir}/${file}`)
//       }
//     }
//   )
// }

export async function splitPDFInPages (fileName, destDir) {
  const pdf = await fs.readFile(fileName)
  const pages = await splitPDF(pdf)
  const res = []
  let idx = 0
  for (const p of pages) {
    idx += 1
    try {
      fs.writeFileSync(`${destDir}/page-${idx}.pdf`, p)
      res.push(`${destDir}/page-${idx}.pdf`)
      console.log('Listo pátina', idx)
    } catch (error) {
      console.log('error', error)
    }
  }
  return res
}
